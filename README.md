# get_pubs.py

This script is used to retrieve faculty publications from Pubmed and attempts
to find their corresponding author from a faculty directory lookup table.
Articles published by faculty at UNM HSC are retrieved using the Pubmed query
specified in `query.txt`.  This query can be modified to increase the
sensitivity/specificity of the search results.  Articles returned by the query
are then matched to faculty listed in `faculty.csv` by simply attempting to
match on last name, first and middle initials. This procedure may return false
positives if the author and faculty member share the same last name and
initials. For example the author Smith J will match both Jane Smith and John
Smith. As a result, the final list of publications should be reviewed manually.
The faculty directory (`faculty.csv`) should be updated periodically to ensure
new faculty members are included and faculty who are no longer at UNM are
excluded. 

```
usage: get_pubs.py [-h] [--query QUERY] [--faculty FACULTY] [--out OUT]
                   [--begin BEGIN] [--end END]

optional arguments:
  -h, --help         show this help message and exit
  --query QUERY      pubmed query file location
  --faculty FACULTY  faculty directory file location
  --out OUT          output file location
  --begin BEGIN      pubmed query begin data e.g. 2018/01/01
  --end END          pubmed query end date e.g. 2018/12/31
```

# Example

```
# get all publications between 2019/01/01 and 2019/12/31
./get_pubs.py --begin 2019/01/01 --end 2019/12/31 --out 2019_results.csv

# use a different faculty directory
./get_pubs.py --begin 2019/01/01 --end 2019/12/31 --faculty ./path/faculty_directory.csv --out pubications.csv
```
