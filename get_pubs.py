#!/usr/bin/env python3.7

import pandas as pd
from urllib import request, parse
import xml.etree.ElementTree as ET
import argparse
import re

# regex for parsing name from pubmed author data
middle = re.compile('^[^ ]* [A-Z]([A-Z])?')
first = re.compile('^[^ ]* ([A-Z])[A-Z]?')
last = re.compile('^([^ ]*)')

def build_pubmed_query(query_file, begindate, enddate):
    """
    Read in Pubmed query from the query_file location and encode it to make it safe to pass
    to the Pubmed Entrez API
    """
    query = ""
    for line in open(query_file):
        if not line.startswith('#'):
            query += line.strip()
    return parse.quote(query.format(begindate, enddate))

def fetch_pubmed_results(query):
    """
    Query Pubmed and returns a Pandas data frame of the results
    """
    esearch = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term={}&usehistory=y$retmax=100000"
    resp = ET.fromstring(request.urlopen(esearch.format(query)).read().decode('utf-8'))
    web_env = resp.find('WebEnv').text
    key = resp.find('QueryKey').text
    efetch="https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&query_key={}&WebEnv={}&retmode=text&rettype=csv&retmax=100000"
    df = pd.read_csv(efetch.format(key,web_env))
    cols = df.columns
    df = df.reset_index().drop('Properties', axis=1)
    df.columns = cols
    return df 

def restructure_publications(pubs):
    """
    Format Pubmed data frame so that each author is represented as a row
    """
    tmp = (
        pd.DataFrame(pubs.Description.str.split(',').tolist(), index = pubs.URL)
        .stack()
        .reset_index()
        .rename(columns={0:'Author'})
        .drop('level_1',axis=1)
        .set_index('URL')
        )
    
    return (
        pubs.set_index('URL')
        .join(tmp)
        .assign(Author = lambda x: x.Author.map(lambda x: x.strip()))
        .assign(first_init = lambda x: x.Author.map(lambda x: parse_names(x)[0]))
        .assign(middle_init = lambda x: x.Author.map(lambda x: parse_names(x)[1]))
        .assign(last_name = lambda x: x.Author.map(lambda x: parse_names(x)[2]))
    )

def parse_names(x):
    """
    Parse first, and middle initials and last name from the Pubmed results
    """
    x = x.strip()
    try:
        last_name = last.match(x).group(1)
    except:
        last_name = None
    try:
        first_init = first.match(x).group(1) 
    except:
        first_init = None
    try:
        middle_init = middle.match(x).group(1) 
    except:
        middle_init = None
    return first_init, middle_init, last_name

def join_faculty_to_pubs(fac, pub):
    """
    Merge the Faculty directory data frame with the Pubmed results
    """
    pub = (
        pub
        .assign(last_name = lambda x: x.last_name.str.upper())
        .set_index('last_name')
        .join(fac.assign(LAST_NAME = lambda x: x.LAST_NAME.str.upper()).set_index('LAST_NAME'), how='inner')
    )
    mask = (
        (pub.first_init.str.upper() == pub.FIRST_NAME.str.upper().map(lambda x: x[0])) &
        (
            (pub.middle_init.str.upper() == pub.MIDDLE_INITIAL.str.upper()) |
            (
                (pub.middle_init.isna()) |
                (pub.MIDDLE_INITIAL.isna())
            )
        )
    )
    results = pub.loc[mask].reset_index().rename(columns = {'index':'LAST_NAME'})
    results = results.filter(['DEPARTMENT','FIRST_NAME','MIDDLE_INITIAL','LAST_NAME','Author','Title','Description','ShortDetails'])
    cols = [i.upper() for i in results.columns]
    results.columns = cols
    results.DEPARTMENT = results.DEPARTMENT.str.title()
    results.FIRST_NAME = results.FIRST_NAME.str.title()
    results.LAST_NAME = results.LAST_NAME.str.title()    
    return results.sort_values('DEPARTMENT')

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--query', help = 'pubmed query file location', default = './query.txt')
    parser.add_argument('--faculty', help = 'faculty directory file location', default='./faculty.csv')
    parser.add_argument('--out', help = 'output file location', default = './pubmed-results.csv')
    parser.add_argument('--begin', help = 'pubmed query begin data e.g. 2018/01/01', default = '2018/01/01')
    parser.add_argument('--end', help = 'pubmed query end date e.g. 2018/12/31', default = '2018/12/31')

    args = parser.parse_args()

    print('Searching Pubmed for articles matching {} published between {} and {}'.format(args.query, args.begin, args.end))
    faculty = pd.read_csv(args.faculty)
    query = build_pubmed_query(args.query, args.begin, args.end)
    pub_recs = fetch_pubmed_results(query)
    n_articles = pub_recs.shape[0]
    print('Query returned {} results'.format(n_articles))

    pub_recs = restructure_publications(pub_recs)    
    res = join_faculty_to_pubs(faculty, pub_recs)
    print('{} coauthorships were found among the {} articles'.format(res.shape[0], n_articles)) 

    print('Saving results to {}'.format(args.out))
    res.to_csv(args.out, index=False)
